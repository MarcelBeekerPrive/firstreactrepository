﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FirstReactAPI.Models {
  public class User {

    public short Id { get; set; }

    public string FirstName { get; set; }
    public string LastName { get; internal set; }
  }
}