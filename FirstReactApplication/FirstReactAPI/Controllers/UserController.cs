﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using FirstReactAPI.Models;

namespace FirstReactAPI.Controllers
{
  //[System.Web.Mvc.Route("api/[controller]")]
  [EnableCors(origins: "*", headers: "*", methods: "*")]
  public class UserController : ApiController {
    // GET: User
    public List<User> GetAll() {

      var user = new User() {
        Id = 1,
        FirstName = "Marcel",
        LastName = "Beeker"
      };

      return new List<User>() { user }; 
    }
  }
}