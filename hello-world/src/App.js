import React, { Component } from 'react';
import './App.css';
import Clock from './Clock';
import ApiClient from './ApiClient';

class App extends Component {
  render() {
    return (
      <div>
        <Clock />
        <button onClick={handleClick}>
          Click me
        </button>
        <Greeting isLoggedIn={true} />
        <ApiClient />
      </div>
    );
  }
}

function handleClick(e) {
  e.preventDefault();
  console.log('The link was clicked.');
}

function Greeting(props) {
  const isLoggedIn = props.isLoggedIn;
  if (isLoggedIn) {
    return <UserGreeting />;
  }
  return <GuestGreeting />;
}

function UserGreeting(props) {
  return <h1>Welcome back!</h1>;
}

function GuestGreeting(props) {
  return <h1>Please sign up.</h1>;
}

export default App;
