import React, { Component } from 'react';

const APIURL = 'http://localhost:53488/api/user/getall';

var indents = [];

class ApiClient extends Component {
  render() {
    return (
      <div>
        <button onClick={invokeWebApi}>
          Invoice WebApi call
        </button>
        {indents}
        "Some text value"
      </div>
    )
  }  
};

function invokeWebApi() {
  return fetch(APIURL, {
        headers: {
          'Accept': 'application/json',
          'Access-Control-Allow-Origin':'*',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
        }
      })
      .then(parseJSON)
      .then(data => {
        data.forEach(function(element) {
          console.log('Id: ' + element.Id + ' FirstName: ' + element.FirstName + ' LastName: ' + element.LastName);

          indents.push(<span key={element.Id}> element.LastName</span>);
        });
      })
      .catch(handleApiError);
}
 

function parseJSON(response) {
  return response.json()
}

function handleApiError(error) {
  console.log('request failed', error)
}

export default ApiClient;